<?php
/**
 * Plugin Name:  	    Rosenkilde-Plugin
 * Description:  	    An Awesome Plugin by Rosenkilde
 * Version:             1.0.0
 * Requires Elementor:  2.0.0
 * Requires PHP:        7.4
 * Author:       	    Lars Rosenkilde Vittrup
 * Author URI:   	    https://gitlab.com/lrv5
 * Text Domain:  	    rosenkilde-plugin
 * Plugin URI:   	    https://gitlab.com/lrv5/rosenkilde-plugin
 *
 * @package rosenkilde-plugin
 * @author Rosenkilde
 */

namespace Rosenkilde_Plugin;

if ( ! defined( 'WPINC' ) ) {
    die();
}

if ( ! class_exists( 'Rosenkilde_Plugin' ) ) {
    /**
     * Sets up and initializes the plugin.
     */
    class Rosenkilde_Plugin {

        const VERSION = "1.0.0";
        const ELEMENTOR_MIN_VERSION = '2.0.0';

        /**
         * A reference to an instance of this class.
         *
         * @since  1.0.0
         * @access private
         * @var    object
         */
        private static $_instance;

        /**
         * Holder for base plugin path
         *
         * @since  1.0.0
         * @access private
         * @var    string
         */
        private $plugin_path = null;

        /**
         * Sets up needed actions/filters for the plugin to initialize.
         *
         * @since 1.0.0
         * @access public
         * @return void
         */
        public function __construct() {
            // Load files.
            add_action( 'init', array( $this, 'init' ), -999 );
//            add_action( 'init', array( $this, 'new_category') );

            add_action( 'elementor/elements/categories_registered', array( $this, 'new_category' ) );

            // Register activation and deactivation hook.
            register_activation_hook( __FILE__, array( $this, 'activation' ) );
            register_deactivation_hook( __FILE__, array( $this, 'deactivation' ) );
        }

        /**
         * Manually init required modules.
         *
         * @return void
         */
        public function init() {
            $this->load_files();

            $this->register_controls();
            $this->register_widgets();
        }

        /**
         * Load required files.
         *
         * @return void
         */
        public function load_files() {
            require $this->plugin_path( '/src/Controls/rosenkilde_plugin_control.php' );
            require $this->plugin_path( '/src/Widgets/article_plugin_widget.php' );
        }

        /**
         * Register widgets.
         *
         * @return void
         */
        public function register_widgets() {
            article_plugin_widget()->init();
        }

        /**
         * Register controls.
         *
         * @return void
         */
        public function register_controls() {
            rosenkilde_control()->init();
        }

        /**
         * Returns path to file or dir inside plugin folder
         *
         * @param string|null $path Path inside plugin dir.
         * @return string
         */
        public function plugin_path(string $path = null ): string {
            if ( ! $this->plugin_path ) {
                $this->plugin_path = trailingslashit( plugin_dir_path( __FILE__ ) );
            }
            return $this->plugin_path . $path;
        }

        public function new_category( $elements_manager ) {
            $elements_manager->add_category( 'custom_category', [
                    'title' => __( 'Rosenkilde Menu', 'rosenkilde-plugin' ),
                    'icon' => 'fa fa-plug',
                ]
            );
        }

        /**
         * Do some stuff on plugin activation
         *
         * @since  1.0.0
         * @return void
         */
        public function activation() {
        }

        /**
         * Do some stuff on plugin deactivation
         *
         * @since  1.0.0
         * @return void
         */
        public function deactivation() {
        }


        /**
         * Returns the instance.
         *
         * @since  1.0.0
         * @access public
         * @return object
         */
        public static function instance(): self {
            if ( self::$_instance == null) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
    }
}

if ( ! function_exists( 'rosenkilde_plugin' ) ) {
    /**
     * Returns instance of the plugin class.
     *
     * @since  1.0.0
     * @return object
     */
    function rosenkilde_plugin() {
        return Rosenkilde_Plugin::instance();
    }
}

rosenkilde_plugin();